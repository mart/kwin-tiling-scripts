/*
    KWin - the KDE window manager
    This file is part of the KDE project.

    SPDX-FileCopyrightText: 2022 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

let ignore = readConfig("Ignore", false);
let ignoreList = ["plasmashell", "krunner"];
if (ignore) {
    ignoreList.concat(readConfig("IgnoreList", "vlc, xv, vdpau, smplayer, dragon, xine, ffplay, mplayer").toString().toLowerCase().split(","));
}

for (i = 0; i < ignoreList.length; ++i) {
    ignoreList[i] = ignoreList[i].trim();
}

function ignoreClient(client) {
    if (client.minimized || client.transient || !client.normalWindow || !client.managed) {
        return true;
    } else if (ignoreList.indexOf(client.resourceClass.toString()) >= 0) {
        return true;
    } else {
        return false;
    }
}

function searchBiggestLeafTile(screenIdx) {
    const tiling = workspace.tilingForScreen(screenIdx);

    if (tiling.rootTile.tiles.length === 0) {
        if (tiling.rootTile.tiles.length === 0) {
            return tiling.rootTile;
        }
    }

    let stack = [tiling.rootTile];
    let biggest = tiling.rootTile;
    biggest = null;

    while (stack.length > 0) {
        let candidate = stack[0];
        stack.splice(0, 1);
        if (!candidate.isLayout) {
            // >= to prefer splitting tiles more recent in the layout
            if (biggest === null
                || (candidate.absoluteGeometry.width * candidate.absoluteGeometry.height) >= (biggest.absoluteGeometry.width * biggest.absoluteGeometry.height)) {
                biggest = candidate;
            }
        } else {
            stack = stack.concat(candidate.tiles);
        }
    }

    return biggest;
}

function searchEmptyTile(screenIdx) {
    const tiling = workspace.tilingForScreen(screenIdx);

    if (tiling.rootTile.tiles.length === 0) {
        if (tiling.rootTile.windows.length === 0) {
            return tiling.rootTile;
        } else {
            //return null;
        }
    }

    let stack = [tiling.rootTile];

    while (stack.length > 0) {
        let candidate = stack[0];
        stack.splice(0, 1);

        if (!candidate.isLayout) {
            if (candidate.windows.length === 0) {
                return candidate;
            }
        } else {
            stack = stack.concat(candidate.tiles);
        }
    }

    return null;
}

function lastTile(screenIdx) {
    const tiling = workspace.tilingForScreen(screenIdx);
    let stack = [tiling.rootTile];
    let candidate = tiling.rootTile;

    while (stack.length > 0) {
        candidate = stack[0];
        stack.splice(0, 1);

        if (candidate.isLayout) {
            for (let i in candidate.tiles) {
                stack.push(candidate.tiles[i]);
            }
        }
    }

    // Fail when tiles get too small
    if (candidate.absoluteGeometry.width < 400 || candidate.absoluteGeometry.height < 400) {
        return null;
    }
    return candidate;
}

// TODO: merge somehow with tileWindowAtPosition
function assignTileToWindow(tile, client) {
    if (tile.windows.length == 0) {
        client.tile = tile;
        return;
    }

    // Fail when tiles get too small
    if (tile.absoluteGeometry.width < 400 || tile.absoluteGeometry.height < 400) {
        client.tile = tile;
        return;
    }
    const tiling = workspace.tilingForScreen(client.output);

    // Not a Custom tile?
    if (!tile.split) {
        client.tile = tile;
        return;
    }

    let splitTiles = null;

    if (tile === tiling.rootTile || tile.parent.layoutDirection === 2) {
        splitTiles = tile.split(1);
    } else {
        splitTiles = tile.split(2);
    }
print("assignTileToWindow"+tile+"["+splitTiles+"]")

    client.tile = splitTiles[1];

}

function tileWindowAtPosition(client, pos) {
    const tiling = workspace.tilingForScreen(client.output);
    let tile = tiling.bestTileForPosition(pos.x, pos.y);
    if (!tile) {
        assignTileToWindow(tiling.rootTile, client);
        return;
    }
    // Distance from center
    const distanceX = pos.x - (tile.absoluteGeometry.x + tile.absoluteGeometry.width / 2);
    const distanceY = pos.y - (tile.absoluteGeometry.y + tile.absoluteGeometry.height / 2);

    if (distanceX > tile.absoluteGeometry.width / 4 && distanceX >= distanceY) {
        let windows = [...tile.windows];
        let splitTiles = tile.split(1);
        client.tile = splitTiles[1];
        for (let win of windows) {
            win.tile = splitTiles[0]
        }
    } else if (distanceX < -tile.absoluteGeometry.width / 4 && distanceX <= distanceY) {
        let windows = [...tile.windows];
        let splitTiles = tile.split(1);
        client.tile = splitTiles[0];
        for (let win of windows) {
            win.tile = splitTiles[1]
        }
    } else if (distanceY > tile.absoluteGeometry.height / 4 && distanceY > distanceX) {
        let windows = [...tile.windows];
        let splitTiles = tile.split(2);
        client.tile = splitTiles[1];
        for (let win of windows) {
            win.tile = splitTiles[0]
        }
    } else if (distanceY < -tile.absoluteGeometry.height / 4 && distanceY < distanceX) {
        let windows = [...tile.windows];
        let splitTiles = tile.split(2);
        client.tile = splitTiles[0];
        for (let win of windows) {
            win.tile = splitTiles[1]
        }
    } else {
        client.tile = tile;
    }
}

var windowAdded = function(client) {

    if (ignoreClient(client)) {
        return;
    }

    if (!("interactiveMoveResizeTracker" in client)) {
        client.moveResizedChanged.connect(() => {
            workspace.hideOutline();

            if (!client.tile && !client.move) {
                tileWindowAtPosition(client, workspace.cursorPos);
            }
        });

        // TODO: add maximizeMode property to Window
        client.maximizedAboutToChange.connect((mode) => {
            client.maximizeTracking = mode;
        });

        client.maximizedChanged.connect(() => {
            if (client.maximizeTracking == 0) {
                const posX = client.frameGeometry.x + client.frameGeometry.width/2;
                const posY = client.frameGeometry.y + client.frameGeometry.height/2;
                tileWindowAtPosition(client, {"x": posX, "y": posY});
            }
        });

        client.frameGeometryChanged.connect(() => {
            if (!client.move) {
                return;
            }

            const tiling = workspace.tilingForScreen(client.output);
            let tile = tiling.bestTileForPosition(workspace.cursorPos.x, workspace.cursorPos.y);
            if (!tile) {
                return;
            }

            let geom = tile.absoluteGeometry
            workspace.showOutline(geom.x, geom.y, geom.width, geom.height);
        });

        client.maximizeTracking = 0;
        client.interactiveMoveResizeTracker = true;
    }

    client.oldTile = client.tile;
    if (!("tileWatcher" in client)) {
        client.tileChanged.connect((tile) => {
            if (client.oldTile && client.oldTile.isLayout == 0 && client.oldTile.windows.length == 0) {
                client.oldTile.remove();
            }
            client.oldTile = tile;
        });

        client.tileWatcher = true;
    }

    const tiling = workspace.tilingForScreen(client.output);

    let tile = searchEmptyTile(client.output);

    if (!tile) {
        tile = searchBiggestLeafTile(client.output);
    }

    if (!tile) {
        tile = tiling.bestTileForPosition(client.frameGeometry.x + client.frameGeometry.width/2, client.frameGeometry.y + client.frameGeometry.height/2);
    }

    assignTileToWindow(tile, client);
};

var windowRemoved = function(client) {
    const tile = client.tile;
    if (tile && tile.windows.length === 1) {
        tile.remove();
    }
};

workspace.windowAdded.connect(windowAdded);
workspace.windowRemoved.connect(windowRemoved);
/*workspace.clientMinimized.connect(windowRemoved);
workspace.clientUnminimized.connect((client) => {
    if (!client.minimized) {
        windowAdded(client);
    }
});*/

let clients = workspace.stackingOrder;
for (let i = 0; i < clients.length; i++) {
  //  windowAdded(clients[i]);
}
