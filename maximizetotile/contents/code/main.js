/*
    KWin - the KDE window manager
    This file is part of the KDE project.

    SPDX-FileCopyrightText: 2022 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

let ignore = readConfig("Ignore", true);
let ignoreList = [];
if (ignore) {
    ignoreList = readConfig("IgnoreList", "vlc, xv, vdpau, smplayer, dragon, xine, ffplay, mplayer").toString().toLowerCase().split(",");
}

for (i = 0; i < ignoreList.length; ++i) {
    ignoreList[i] = ignoreList[i].trim();
}

function ignoreClient(client) {
    if (client.minimized || client.transient) {
        return true;
    } else if (ignoreList.indexOf(client.resourceClass.toString()) >= 0) {
        return true;
    } else {
        return false;
    }
}


var clientMaximizeSet = function(client, h, v) {
    if (ignoreClient(client)) {
        return;
    }

    if (h && v) {
        const tiling = workspace.tilingForScreen(client.output);
        client.tile = client.tileToMaximize;
    } else {
        client.tile = null;
    }
};

var windowAdded = function(client) {
    if (ignoreClient(client)) {
        return;
    }

    const tiling = workspace.tilingForScreen(client.output);

    client.maximizedAboutToChange.connect((state) => {
        client.tileToMaximize = tiling.bestTileForPosition(
            client.frameGeometry.x - tiling.rootTile.absoluteGeometry.x + client.frameGeometry.width/2,
            client.frameGeometry.y - tiling.rootTile.absoluteGeometry.y + client.frameGeometry.height/2);
            client.futureMaximize = state
    });
    client.maximizedChanged.connect(() => {
        let state = client.futureMaximize;
        clientMaximizeSet(client, state & 2, state & 1);
    });
};


workspace.windowAdded.connect(windowAdded);

let clients = workspace.stackingOrder;
for (let i = 0; i < clients.length; i++) {
    windowAdded(clients[i]);
}
