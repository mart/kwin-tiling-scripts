/*
    KWin - the KDE window manager
    This file is part of the KDE project.

    SPDX-FileCopyrightText: 2022 Marco Martin <mart@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

let ignore = readConfig("Ignore", false);
let ignoreList = ["plasmashell", "krunner"];
if (ignore) {
    ignoreList.concat(readConfig("IgnoreList", "vlc, xv, vdpau, smplayer, dragon, xine, ffplay, mplayer").toString().toLowerCase().split(","));
}

for (i = 0; i < ignoreList.length; ++i) {
    ignoreList[i] = ignoreList[i].trim();
}

function ignoreClient(client) {
    if (client.minimized || client.transient) {
        return true;
    } else if (ignoreList.indexOf(client.resourceClass.toString()) >= 0) {
        return true;
    } else {
        return false;
    }
}


var windowAdded = function(client) {

    if (ignoreClient(client)) {
        return;
    }

    const tiling = workspace.tilingForScreen(client.screen);

    switch (client.resourceClass.toString()) {
    case "vlc":
        client.tile = tiling.rootTile.tiles[2];
        break;
    case "konsole":
        client.tile = tiling.rootTile.tiles[0];
        break;
    case "dolphin":
        client.tile = tiling.rootTile.tiles[1];
        break;
    }
};


workspace.windowAdded.connect(windowAdded);


let clients = workspace.stackingOrder;
for (let i = 0; i < clients.length; i++) {
  //  windowAdded(clients[i]);
}
